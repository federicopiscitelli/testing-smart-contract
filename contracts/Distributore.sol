// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;


contract Distributore {

    // Dichiara le variabili di stato del contratto
    address public owner;
    mapping (address => uint) public cupcakeBalances;

    constructor(address prop, uint balance) {
        owner = prop;
        cupcakeBalances[owner] = balance;
    }

    // Consenti al proprietario di aumentare l'amount di cupcake del contratto
    function refill(address sender, uint amount) public {
        require(sender == owner, "Solo il proprietario puo rifornire.");
        require(amount >= 1, "Devi caricare almeno un prodotto.");

        cupcakeBalances[owner] += amount;
    }

    // Consenti a chiunque di comprare cupcake
    function purchase(address customer, uint balance, uint amount) public payable {
        require(amount >= 1, "Devi acquistare almeno un prodotto.");
        require(cupcakeBalances[owner] >= amount, "Non ci sono abbastanza cupcake per completare questo acquisto.");

        cupcakeBalances[owner] -= amount;
        cupcakeBalances[customer] += amount;
    }
}