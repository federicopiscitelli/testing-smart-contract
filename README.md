# Smart contract testing con Waffle JS

Scaricare la repo ed eseguire il comando

```shell
npm install
```

Al fine di semplificare lo sviluppo di questo progetto demo è stato usato Hardhat.
Ecco una lista di semplici comandi:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```

All'interno della cartella docs si possono trovare le slides della presentazione e un file .docx contenente un riassunto dell'argomento presentato.
