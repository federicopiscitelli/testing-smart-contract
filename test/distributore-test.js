const { expect } = require("chai");
const { ethers } = require("hardhat");
const {utils} = require('ethers');
const {MockProvider} = require('ethereum-waffle');

describe("Distributore", function () {

  const [owner, customer] = new MockProvider().getWallets();
  let ownerAddress, customerAddress;
  let Distributore;
  let distributore;

  beforeEach(async () => {
    ownerAddress = await owner.getAddress()
    customerAddress = await customer.getAddress()
    Distributore = await ethers.getContractFactory("Distributore")
    distributore = await Distributore.deploy(ownerAddress, 1000);
    await distributore.deployed()
  });
  
  describe("Inizializzazione", function(){
    it("Owner inizializzato correttamente", async function () {
      expect(await distributore.owner()).to.be.equal(ownerAddress)
    });
  
    it("Quantità inizializzata correttamente", async function () {
      expect(await distributore.cupcakeBalances(ownerAddress)).to.be.equal(1000)
    });
  })
 
  describe("Funzionamento", function(){
    it("Refill da parte dell'owner", async function () {
      await distributore.refill(ownerAddress, 100)
      expect(await distributore.cupcakeBalances(ownerAddress)).to.be.equal(1100)
    });

    it("Acquisto da parte del customer", async function () {
      const initialBalance = ethers.BigNumber.from(await customer.getBalance())
      await distributore.purchase(customerAddress,initialBalance, 1)

      expect(await distributore.cupcakeBalances(customerAddress)).to.be.equal(1)
      expect(await distributore.cupcakeBalances(ownerAddress)).to.be.equal(999)  
    });

  });

  describe("Eccezioni", function(){
    it("Refill da parte del customer", async function () {
      await expect(distributore.refill(customerAddress, 100)).to.be.revertedWith("Solo il proprietario puo rifornire.")
    });

    it("Refill con amount nullo", async function () {
      await expect(distributore.refill(ownerAddress, 0)).to.be.revertedWith("Devi caricare almeno un prodotto.")
    });

    it("Refill con amount negativo", async function () {
      await expect(distributore.refill(ownerAddress, 0)).to.be.reverted
    });

    it("Acquisto con amount nullo", async function () {
      await expect(distributore.purchase(customerAddress,await customer.getBalance(), 0)).to.be.revertedWith("Devi acquistare almeno un prodotto.")
    });

    it("Acquisto con amount negativo", async function () {
      await expect(distributore.purchase(customerAddress,await customer.getBalance(), -1)).to.be.reverted
    });

  });

});
